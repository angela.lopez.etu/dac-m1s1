# TP 1 : Prise en main d’Openstack


## Connexion SSH
Après avoir générée une paire de clé :

 ```
 ssh ubuntu@172.28.100.46 #instance 1 (serveur)
 ```       

  ```
  ssh ubuntu@172.28.100.62 #instance 2 (client)
  ```   
 

## Envoi de messages avec netcat et capture des paquets
Utilisation de tmux pour lancer plusieurs onglets de terminal dans la même session ssh :    
``$tmux``    
serveur 1 : lancement de la capture de paquets en provenance ou à destination du serveur 2 et sauvegarde dans un fichier
``sudo tcpdump -i ens3 host 172.28.100.62 -w capture_file_inst1``

serveur 2 : lancement de la capture de paquets en provenance ou à destination du serveur 1 et sauvegarde dans un fichier    

``sudo tcpdump -i ens3 host 172.28.100.46 -w capture_file_inst2``

serveur 2 écoute :

`` sudo netcat -l -p 30 ``  

serveur 1 envoie les messages

``sudo nc  172.28.100.62 30``    

Récupération des fichiers sur les serveurs distants :    
```
scp ubuntu@172.28.100.62:/home/ubuntu/capture_file_inst2  capture_file_inst2.pcap 
```
 ```
 scp ubuntu@172.28.100.46:/home/ubuntu/capture_file_inst1  capture_file_inst1.pcap
 ```



![capture wireshark côté serveur 1](./server1_capture.png)
![contenu du paquet utile côté serveur 1](./tcp_packet_content.png)    

On voit bien le protocole de résolution d'adresse (ligne 0 et 1) puis le 3-way handshake (ligne 2 à 4) puis le contenu effectif envoyé avec netcat.    


## Mise en place d'un firewall 

Dans la session ssh de l'instance 2 : 

```
ufw allow ssh #on veut conserver notre session ssh
ufw default deny incoming #toutes les autres connexions seront refusées
ufw enable 
```


