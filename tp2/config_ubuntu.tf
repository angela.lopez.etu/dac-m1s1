terraform {
  required_providers {
    openstack = {
      source = "terraform-provider-openstack/openstack"
      version = "1.43.1"
    }
  }
}

resource "openstack_compute_keypair_v2" "test-keypair" {
  provider = openstack
  name       = "ssh-key-m5"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDIFm7rxgMok23GOye2J9G0YpdJnRgX1aIHMu4143AJEIrroZV54fC01NLhjQMeWdsVlv6pvzRk6fjaOpq+T1sBJN3s2iVUYp9YpQNkzFCDAoBwSU8i69jrR0ctMR0ohHwgG6vh83aAU6n9d5EQSI+R+Z1j6x9WiLgTVtULnaGLL793Y9rPT1Pgagw2qcSZhToC93Oor7E8xsV+VKBk13PCfLkG6ZipvGEYMIVqZvY5SZaXH5TcuI9N0oEYrHvwk94gaZsfmqVtWAg/DdU5R9PoYwZS+eH87x9y+QHV2V8KOHvdaHc72CWUvWOINF0Cayj/h23inOjq9qxbhweaTVUD"
}

resource "openstack_compute_instance_v2" "terraform_ubuntu_instances" {
    count = 4
    name = "terraform_ubuntu_${count.index}"
    provider = openstack
    flavor_name = "petite"
    key_pair = openstack_compute_keypair_v2.test-keypair.name
    image_name = "ubuntu-20.04"
}


